FROM haskell:8.10
RUN cabal update
RUN git clone https://github.com/kowainik/stan.git && \
cd stan && \
cabal v2-build exe:stan
RUN cd stan && \
cp "$(cabal v2-exec --verbose=0 --offline sh -- -c 'command -v stan')" /usr/local/bin/stan
COPY hello.cabal .
RUN stan